# lila clone
This repo is a lesser clone of the upstream lichess.org lila
project available here:

* https://github.com/ornicar/lila/

See README-upstream.md in this directory.

# Installation
See this doc:

* https://github.com/ornicar/lila/wiki/Lichess-Development-Onboarding

# License
Same as upstream, AGPL 3.

