package views.html.base

import controllers.routes

import lila.api.Context
import lila.app.templating.Environment._
import lila.app.ui.ScalatagsTemplate._

object topnav {

  private def linkTitle(url: String, name: Frag)(implicit ctx: Context) =
    if (ctx.blind) h3(name) else a(href := url)(name)

  private def canSeeClasMenu(implicit ctx: Context) =
    ctx.hasClas || ctx.me.exists(u => u.hasTitle || u.roles.contains("ROLE_COACH"))

  def apply()(implicit ctx: Context) =
    st.nav(id := "topnav", cls := "hover")(
      st.section(
        linkTitle(("/player/bots"), ("Bots")),
        div(role := "group")(
          a(href := "/games/bot")(trans.currentGames())
        )
      ),
      st.section(
        linkTitle(("https://spacecruft.org/deepcrayon/lila"), ("Code")),
        div(role := "group")(
          a(href := "https://github.com/ornicar/lila")("Upstream")
        )
      )
    )
}
