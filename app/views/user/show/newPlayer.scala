package views.html.user.show

import lila.app.templating.Environment._
import lila.app.ui.ScalatagsTemplate._
import lila.user.User

import controllers.routes

object newPlayer {

  def apply(u: User) =
    div(cls := "new-player")(
      h2("Bienvenido a DeepCrayonFish!"),
      p(
        "This is your profile page.",
        u.profile.isEmpty option frag(
          br,
          "Would you like to ",
          a(href := routes.Account.profile)("improve it"),
          "?"
        )
      ),
      p(
        if (u.kid) "Kid mode is enabled."
        else
          frag(
            "Will a child use this account? You might want to enable ",
            a(href := routes.Account.kid)("Kid mode"),
            "."
          )
      ),
      p(
        "Note: This is a development / test server that will be restarted, wiped, deleted, and permanently disabled at any time. Not for production."
      ),
    )
}
