package views.html
package user

import controllers.routes

import lila.api.Context
import lila.app.templating.Environment._
import lila.app.ui.ScalatagsTemplate._
import lila.user.User

object bots {

  def apply(users: List[User])(implicit ctx: Context) = {

    val title = s"${users.size} Online bots"

    val sorted = users.sortBy { -_.playTime.??(_.total) }

    views.html.base.layout(
      title = title,
      moreCss = frag(cssTag("slist"), cssTag("user.list")),
      wrapClass = "full-screen-force"
    )(
      main(cls := "page-menu bots")(
        user.bits.communityMenu("bots"),
        sorted.partition(_.isVerified) match {
          case (featured@_, all) =>
            div(cls := "bots page-menu__content")(
              div(cls := "box")(
                botTable(all)
              )
            )
        }
      )
    )
  }

  private def botTable(users: List[User])(implicit ctx: Context) = table(cls := "slist slist-pad")(
    tbody(
      users map { u =>
        tr(
          td(userLink(u)),
          u.profile
            .ifTrue(ctx.noKid)
            .ifTrue(!u.marks.troll || ctx.is(u))
            .flatMap(_.nonEmptyBio)
            .map { bio =>
              td(shorten(bio, 400))
            } | td,
          ctx.pref.showRatings option td(cls := "rating")(u.best3Perfs.map {
            showPerfRating(u, _)
          }),
          u.playTime.fold(td) { playTime =>
            td(
              p(
                cls := "text",
                dataIcon := "",
                st.title := trans.tpTimeSpentPlaying.txt(showPeriod(playTime.totalPeriod))
              )(showPeriod(playTime.totalPeriod)),
              playTime.nonEmptyTvPeriod.map { tvPeriod =>
                p(
                  cls := "text",
                  dataIcon := "",
                  st.title := trans.tpTimeSpentOnTV.txt(showPeriod(tvPeriod))
                )(showPeriod(tvPeriod))
              }
            )
          }
        )
      }
    )
  )
}
